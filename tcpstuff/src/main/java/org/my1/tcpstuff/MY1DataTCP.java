package org.my1.tcpstuff;

import java.io.ByteArrayOutputStream;

public class MY1DataTCP {
    private int pack_uuid; // 16-bit unsigned value actually!
    private byte[] pack_data;
    public int pack_ccrc, temp_ccrc;
    static final int UUID_HOST = 0x0000;
    static final int UUID_MASK = 0xffff;
    // 2-byte packet pength + 2-byte UUID (excluding crc16)
    static final int PACK_MINSIZE = 4;
    // not enforced - but can be useful
    private int pack_type; /* first 2 bytes in data if used */
    static final public int PACK_TYPE_TX = 0x1000;
    static final public int PACK_TYPE_RX = 0x2000;
    static final public int PACK_TYPE_OK = 0x4000;
    static final public int PACK_TYPE_QQ = 0x8000;

    public MY1DataTCP() {
        pack_uuid = UUID_HOST;
    }

    public MY1DataTCP(int uuid) {
        pack_uuid = uuid&UUID_MASK;
    }

    public int UUID() {
        return pack_uuid;
    }
    public int Type() {
        return pack_type;
    }
    public int IsRX() {
        return pack_type&PACK_TYPE_RX;
    }
    public int IsTX() {
        return pack_type&PACK_TYPE_TX;
    }
    public int IsOK() {
        return pack_type&PACK_TYPE_OK;
    }
    public byte[] Data() {
        return pack_data;
    }
    public int Data8(int pick) {
        return pack_data[pick];
    }
    public int Data16(int pick) {
        return getWord16(pack_data,pick);
    }
    public int getWord16(byte[] data, int pick) {
        return ((data[pick+1]&0xff)<<8)|(data[pick]&0xff);
    }

    public int crc16_byte(int ccrc, byte data) {
        ccrc ^= (int)data&0xff;
        for (int iter=0;iter<8;iter++) {
            if ((ccrc&0x01)==0x01)
                ccrc = (ccrc>>1)^0x8408;
            else ccrc >>= 1;
        }
        return ccrc;
    }

    public int crc16_calc_core(byte[] buff, int offs, int size) {
        int ccrc = 0;
        for (int loop=offs;loop<size;loop++) {
            ccrc = crc16_byte(ccrc,buff[loop]);
        }
        return ccrc&0xffff;
    }
    public int crc16_calc(byte[] buff, int size) {
        return crc16_calc_core(buff,0,size);
    }

    public void DoPackTX(byte[] data) {
        pack_type = PACK_TYPE_TX;
        DoPackType(data,pack_type);
    }
    public void DoPackType(byte[] data, int type) {
        byte[] buff = new byte[data.length+2];
        buff[0] = (byte) (type&0xff);
        buff[1] = (byte) ((type>>8)&0xff);
        System.arraycopy(data, 0, buff, 2, data.length);
        DoPackRaw(buff);
    }
    public void DoPackRaw(byte[] data) {
        pack_data = DoPack(data);
    }
    public byte[] DoPack(byte[] data) {
        return DoPack(data,data.length);
    }

    public byte[] DoPack(byte[] data, int size) {
        ByteArrayOutputStream pack = new ByteArrayOutputStream();
        int temp = size + PACK_MINSIZE;
        pack.write((byte)(temp&0xff));
        pack.write((byte)((temp>>8)&0xff));
        pack.write((byte)(pack_uuid&0xff));
        pack.write((byte)((pack_uuid>>8)&0xff));
        pack.write(data,0,size);
        pack_ccrc = crc16_calc(pack.toByteArray(),pack.size());
        pack.write((byte)(pack_ccrc&0xff));
        pack.write((byte)((pack_ccrc>>8)&0xff));
        pack_data = pack.toByteArray();
        return pack_data;
    }

    public void UnPackType(byte[] data) {
        byte[] temp = UnPack(data);
        if ((temp!=null)&&(temp.length>2)) {
            pack_type = ((int)temp[1]<<8)|temp[0];
            pack_data = new byte[temp.length-2];
            System.arraycopy(temp, 2, pack_data, 0, temp.length-2);
        }
    }

    public byte[] UnPack(byte[] data) {
        return UnPack(data,data.length);
    }

    public byte[] UnPack(byte[] data, int size) {
        pack_uuid = 0;
        pack_data = null;
        pack_type = 0;
        pack_ccrc = 0;
        temp_ccrc = 0;
        if (size>=PACK_MINSIZE+2) {
            int temp = getWord16(data,0);
            if (temp == size - 2) { // temp excludes crc16!
                ByteArrayOutputStream pack = new ByteArrayOutputStream();
                pack.write(data,PACK_MINSIZE,temp-PACK_MINSIZE);
                pack_ccrc = crc16_calc(data,size-2);
                temp_ccrc = getWord16(data,size-2);
                if (temp_ccrc==pack_ccrc) {
                    pack_uuid = getWord16(data,2);
                    pack_data = pack.toByteArray();
                }
            }
        }
        return pack_data;
    }
}
