package org.my1.tcpstuff;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class MY1SendTCP {

    static public final int TCP_STAT_IDLE = 0x0000;
    static public final int TCP_STAT_SENT = 0x0001;
    static public final int TCP_STAT_RECEIVED = 0x0002;
    static public final int TCP_STAT_CONTENT = 0x0004;
    static public final int TCP_STAT_TIMEOUT = 0x0100;
    static public final int TCP_STAT_EXCEPTION = 0x0800;
    static public final long DEFAULT_TIMEOUT_MS = 30000;
    static public final String VERSION = BuildConfig.TCPSTUFF_VNAME +
            " (" + BuildConfig.TCPSTUFF_VCODE + ")";

    private final byte[] thisData;
    private long timeKeep;
    private int sendStat;
    private byte[] thatData;
    private String emesg;

    public MY1SendTCP(byte[] data) {
        thisData = data;
        timeKeep = DEFAULT_TIMEOUT_MS;
        sendStat = TCP_STAT_IDLE;
    }

    public void Timeout(long timeout_ms) { timeKeep = timeout_ms; }
    public boolean DataRecv() {
        return (sendStat & TCP_STAT_CONTENT) == TCP_STAT_CONTENT;
    }
    public boolean Received() {
        return (sendStat & TCP_STAT_RECEIVED) == TCP_STAT_RECEIVED;
    }
    public boolean TimeSkip() {
        return (sendStat & TCP_STAT_TIMEOUT) == TCP_STAT_TIMEOUT;
    }
    public boolean Failed() {
        return (sendStat & TCP_STAT_EXCEPTION) == TCP_STAT_EXCEPTION;
    }
    public byte[] Data() {
        return thatData;
    }
    public String errorMessage() {
        return emesg;
    }
    public String Version() {
        return VERSION;
    }
    public void socketTxRx(String host_addr, String host_port) {
        ByteArrayOutputStream outd = new ByteArrayOutputStream();
        try {
            sendStat = TCP_STAT_IDLE;
            Socket sock = new Socket(host_addr, Integer.parseInt(host_port));
            DataOutputStream outw = new DataOutputStream(sock.getOutputStream());
            outw.write(thisData);
            outw.flush();
            sendStat |= TCP_STAT_SENT;
            DataInputStream inpr = new DataInputStream(sock.getInputStream());
            long ends = System.currentTimeMillis() + timeKeep;
            while (inpr.available() == 0) {
                if (System.currentTimeMillis()>ends) {
                    sendStat |= TCP_STAT_TIMEOUT;
                    break;
                }
            }
            if (inpr.available()>0) {
                sendStat |= TCP_STAT_RECEIVED;
                byte[] buff = new byte[4096];
                while (true) {
                    int size = inpr.read(buff);
                    outd.write(buff, 0, size);
                    if (size < 4096) break;
                }
                if (outd.size() > 0) {
                    sendStat |= TCP_STAT_CONTENT;
                    thatData = outd.toByteArray();
                }
            }
            outw.close();
            inpr.close();
            sock.close();
        }
        catch (Exception e) {
            sendStat |= TCP_STAT_EXCEPTION;
            emesg = e.getMessage();
        }
    }
}
