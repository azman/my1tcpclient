package org.my1.tcpclient;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.my1.tcpstuff.MY1DataTCP;
import org.my1.tcpstuff.MY1SendTCP;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {
    static final int SELFID = 0xACE5;
    static final char[] HEXASCII = "0123456789ABCDEF".toCharArray();
    String host_addr = "127.0.0.1";
    int host_port = 1337;
    Button btInitSend;
    TextView tvSendStat;
    EditText etHostAddr, etHostPort;
    EditText etSendData, etRecvData;

    MY1SendTCP data_send;
    MY1DataTCP data_pack, recv_pack;

    protected void showResults(MY1SendTCP sendTCP) {
        StringBuilder show = new StringBuilder();
        String temp;
        if (sendTCP.Failed()) {
            temp = sendTCP.errorMessage();
            show.append("> FAIL ");
            show.append(temp);
            tvSendStat.setText(show.toString());
            showToast("[Fail] "+temp);
        }
        else if (sendTCP.TimeSkip()) {
            temp = "Socket Timeout";
            show.append("> FAIL ");
            show.append(temp);
            tvSendStat.setText(show.toString());
            showToast("[Fail] "+temp);
        }
        else if (sendTCP.DataRecv()) {
            byte[] data = data_send.Data();
            recv_pack.UnPackType(data);
            if (recv_pack.Data()!=null) {
                show.append("[RECV] ");
                for (byte datum : data) {
                    show.append(HEXASCII[(datum & 0xf0) >> 4]);
                    show.append(HEXASCII[(datum & 0xf)]);
                }
                etRecvData.setText(show.toString());
            }
            else {
                data = recv_pack.Data();
                show.append("UUID:");
                show.append(String.format("%04X",recv_pack.UUID()&0xffff));
                if (recv_pack.Type()>0) {
                    show.append(", Type:");
                    show.append(String.format("%04X",recv_pack.Type()&0xffff));
                }
                show.append(", Data:{");
                for (int loop=0;loop<data.length;loop++) {
                    if (loop>0) show.append(",");
                    show.append(String.format("%02X",data[loop]&0xff));
                }
                show.append("}, CRC16-C:");
                show.append(String.format("%04X",recv_pack.pack_ccrc&0xffff));
                show.append(", CRC16-D:");
                show.append(String.format("%04X",recv_pack.temp_ccrc&0xffff));
                etRecvData.setText(show.toString());
            }
            show.setLength(0);
            show.append("> DONE (");
            show.append(data.length);
            show.append(")");
            tvSendStat.setText(show.toString());
        }
        else if (sendTCP.Received()) showToast("[Fail] No reply");
        else showToast("[Fail] Something failed");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String chk1 = "> THAT";
        setContentView(R.layout.activity_main);
        btInitSend = findViewById(R.id.btInitSend);
        tvSendStat = findViewById(R.id.tvSendStat);
        etHostAddr = findViewById(R.id.etHostAddr);
        etHostPort = findViewById(R.id.etHostPort);
        etSendData = findViewById(R.id.etSendData);
        etRecvData = findViewById(R.id.etRecvData);
        etHostAddr.setText(host_addr);
        etHostPort.setText(String.valueOf(host_port));
        tvSendStat.setText(chk1);

        data_send = null;
        data_pack = new MY1DataTCP(SELFID);
        recv_pack = new MY1DataTCP();

        btInitSend.setOnClickListener(v -> {
            if (data_send==null) {
                // read data from etSendData!
                String buff = etSendData.getText().toString().trim()
                        .replaceAll(" +", " ");
                ByteArrayOutputStream outs = new ByteArrayOutputStream();
                for (String word : buff.split(" ")) {
                    word = word.trim();
                    if (word.isEmpty()) continue;
                    int temp = Integer.parseInt(word, 16);
                    outs.write((byte) (temp & 0xff));
                }
                if (outs.size()>0) {
                    final String chk2 = "> Push";
                    tvSendStat.setText(chk2);
                    data_pack.DoPackTX(outs.toByteArray());
                    new Thread(() -> {
                        data_send = new MY1SendTCP(data_pack.Data());
                        data_send.Timeout(20000);
                        data_send.socketTxRx(etHostAddr.getText().toString(),
                                etHostPort.getText().toString());
                        runOnUiThread(() -> {
                            showToast("Thread for socket done!");
                            showResults(data_send);
                            data_send = null;
                        });
                    }).start();
                    showToast("Thread for socket started!");
                }
                else  showToast("No data to send!");
            }
            else showToast("SocketTxRx in progress...");
        });
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
